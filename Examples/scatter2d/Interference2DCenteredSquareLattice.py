#!/usr/bin/env python3
"""
2D lattice with disorder, centered square lattice
"""
import bornagain as ba
from bornagain import deg, nm, kvector_t

r = 3*nm  # particle radius
a = 25*nm # lattice constant

def get_sample():
    """
    Returns a sample with cylinders on a substrate,
    forming a 2D centered square lattice
    """

    # Define materials
    material_Particle = ba.HomogeneousMaterial("Particle", 0.0006, 2e-08)
    material_Substrate = ba.HomogeneousMaterial("Substrate", 6e-06, 2e-08)
    material_Vacuum = ba.HomogeneousMaterial("Vacuum", 0, 0)

    # Define particles
    ff = ba.FormFactorCylinder(r, r)
    particle_1 = ba.Particle(material_Particle, ff)
    particle_2 = ba.Particle(material_Particle, ff)
    particle_2.setPosition(kvector_t(a/2, a/2, 0))

    # Define composition of particles at specific positions
    basis = ba.ParticleComposition()
    basis.addParticle(particle_1)
    basis.addParticle(particle_2)

    # Define 2D lattices
    lattice = ba.SquareLattice2D(a, 0*deg)

    # Define interference functions
    iff = ba.InterferenceFunction2DLattice(lattice)
    iff_pdf = ba.FTDecayFunction2DCauchy(48*nm, 16*nm, 0)
    iff.setDecayFunction(iff_pdf)

    # Define particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(basis)
    layout.setInterferenceFunction(iff)
    layout.setTotalParticleSurfaceDensity(0.0016)

    # Define layers
    layer_1 = ba.Layer(material_Vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(material_Substrate)

    # Define sample
    sample = ba.MultiLayer()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)

    return sample


def get_simulation(sample):
    beam = ba.Beam(1, 0.1*nm, ba.Direction(0.2*deg, 0))
    detector = ba.SphericalDetector(200, -2*deg, 2*deg, 200, 0, 2*deg)
    simulation = ba.GISASSimulation(beam, sample, detector)
    return simulation


if __name__ == '__main__':
    import ba_plot
    sample = get_sample()
    simulation = get_simulation(sample)
    ba_plot.run_and_plot(simulation)
