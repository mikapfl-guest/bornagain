#!/usr/bin/env python3
"""
Simple example demonstrating how polarized SANS experiments can be
simulated with BornAgain.
"""

import bornagain as ba
from bornagain import angstrom, deg, nm, nm2, kvector_t

# Magnetization of the particle's core material (A/m)
magnetization_core = kvector_t(0, 0, 1e7)


def get_sample():
    """
    Returns a sample with a magnetic core-shell particle in a solvent.
    """

    # Define materials
    magnetic_field = kvector_t(0, 0, 1e7)
    material_Core = ba.HomogeneousMaterial("Core", 6e-06, 2e-08,
                                           magnetic_field)
    material_Shell = ba.HomogeneousMaterial("Shell", 1e-07, 2e-08)
    material_Solvent = ba.HomogeneousMaterial("Solvent", 5e-06, 0)

    # Define form factors
    ff_1 = ba.FormFactorFullSphere(10*nm)
    ff_2 = ba.FormFactorFullSphere(12*nm)

    # Define particles
    particle_1 = ba.Particle(material_Core, ff_1)
    particle_1_position = kvector_t(0, 0, 2*nm)
    particle_1.setPosition(particle_1_position)
    particle_2 = ba.Particle(material_Shell, ff_2)

    # Define core shell particles
    particle = ba.ParticleCoreShell(particle_2, particle_1)

    # Define particle layouts
    layout = ba.ParticleLayout()
    layout.addParticle(particle)
    layout.setTotalParticleSurfaceDensity(0.01)

    # Define layers
    layer = ba.Layer(material_Solvent)
    layer.addLayout(layout)

    # Define sample
    sample = ba.MultiLayer()
    sample.addLayer(layer)

    return sample


def get_simulation(sample):
    """
    Returns a polarized SANS simulation
    """
    # Beam from above (perpendicular to sample):
    beam = ba.Beam(1, 0.4*nm, ba.Direction(90*deg, 0))
    beam.setPolarization(kvector_t(0, 0, 1))

    # Detector opposite to source:
    detPos = 2000  # distance from sample center to detector in mm
    detWid = 500  # detector width in mm
    detPix = 200  # number of pixels per direction
    det = ba.RectangularDetector(detPix, detWid, detPix, detWid)
    det.setPerpendicularToDirectBeam(detPos, detWid/2, detWid/2)
    det.setAnalyzerProperties(kvector_t(0, 0, -1), 1, 0.5)

    return ba.GISASSimulation(beam, sample, det)


if __name__ == '__main__':
    import ba_plot
    sample = get_sample()
    simulation = get_simulation(sample)
    ba_plot.run_and_plot(simulation) # TODO: restore units=ba.Axes.QSPACE)
