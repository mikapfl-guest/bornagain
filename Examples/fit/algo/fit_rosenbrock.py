#!/usr/bin/env python3

import bornagain as ba

def rosenbrock(params):
    x = params["x"].value
    y = params["y"].value
    tmp1 = y - x * x
    tmp2 = 1 - x
    return 100 * tmp1 * tmp1 + tmp2 * tmp2

params = ba.Parameters()
params.add("x", value=-1.2, min=-5.0, max=5.0, step=0.01)
params.add("y", value=1.0, min=-5.0, max=5.0, step=0.01)

minimizer = ba.Minimizer()
result = minimizer.minimize(rosenbrock, params)
print(result.toString())
