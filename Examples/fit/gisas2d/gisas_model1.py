"""
Parametric model of a GISAS simulation.
The idealized sample model consists of dilute cylinders on a substrate.
"""

import bornagain as ba
from bornagain import deg, nm

mat_vacuum = ba.HomogeneousMaterial("Vacuum", 0, 0)
mat_substrate = ba.HomogeneousMaterial("Substrate", 6e-6, 2e-8)
mat_particle = ba.HomogeneousMaterial("Particle", 6e-4, 2e-8)


def get_sample(params):
    cylinder_height = params["cylinder_height"]
    cylinder_radius = params["cylinder_radius"]

    ff = ba.FormFactorCylinder(cylinder_radius, cylinder_height)
    cylinder = ba.Particle(mat_particle, ff)
    layout = ba.ParticleLayout()
    layout.addParticle(cylinder)

    layer_1 = ba.Layer(mat_vacuum)
    layer_1.addLayout(layout)
    layer_2 = ba.Layer(mat_substrate)

    sample = ba.MultiLayer()
    sample.addLayer(layer_1)
    sample.addLayer(layer_2)
    return sample


def get_simulation(params):
    beam = ba.Beam(10**params['lg(intensity)'], 0.1*nm,
                   ba.Direction(0.2*deg, 0))
    det = ba.SphericalDetector(100, -1.5*deg, 1.5*deg, 100, 0, 3*deg)
    sample = get_sample(params)

    simulation = ba.GISASSimulation(beam, sample, det)
    simulation.setBackground(
        ba.ConstantBackground(10**params['lg(background)']))

    return simulation

def start_parameters_1():
    params = ba.Parameters()
    params.add("lg(intensity)", 5)
    params.add("lg(background)", 1)
    params.add("cylinder_height", 6.*nm, min=0.01)
    params.add("cylinder_radius", 6.*nm, min=0.01)
    return params
