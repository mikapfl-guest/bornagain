# BornAgain packaging

include(InstallRequiredSystemLibraries)

set(CPACK_PACKAGE_VENDOR "Forschungszentrum Juelich GmbH")

configure_file(COPYING LICENSE.txt COPYONLY)
set(CPACK_RESOURCE_FILE_LICENSE ${CMAKE_BINARY_DIR}/LICENSE.txt)

set(CPACK_PACKAGE_RELOCATABLE True)

set(CPACK_SOURCE_PACKAGE_FILE_NAME ${CMAKE_PROJECT_NAME}-${CMAKE_PROJECT_VERSION})
set(CPACK_PACKAGE_INSTALL_DIRECTORY ${CPACK_SOURCE_PACKAGE_FILE_NAME})

set(CPACK_PACKAGE_FILE_NAME ${CPACK_PACKAGE_INSTALL_DIRECTORY}-${BORNAGAIN_ARCHITECTURE})
if(NOT CMAKE_BUILD_TYPE STREQUAL Release)
    string(APPEND CPACK_PACKAGE_FILE_NAME -${CMAKE_BUILD_TYPE})
endif()


if(WIN32)
    include(BornAgain/PackWindows)
elseif(APPLE)
    include(BornAgain/PackMacOS)
elseif(UNIX)
    if(BUILD_DEBIAN) # one can build debian package only on UNIX system
        include(BornAgain/PackDebian)
    endif()
    include(BornAgain/PackSource)
endif()

message(STATUS "Installer name: ${CPACK_PACKAGE_FILE_NAME}")
message(STATUS "Source package name: ${CPACK_SOURCE_PACKAGE_FILE_NAME}")

include(CPack)

set(CPACK_COMPONENTS_ALL Libraries Headers Examples)
