# Pack BornAgain source archive
#
# To create the .tar.gz file, run "make package_source".

set(CPACK_ARCHIVE_FILE_NAME ${CMAKE_PROJECT_NAME}-${CMAKE_PROJECT_VERSION}.tgz)

set(CPACK_SOURCE_GENERATOR "TGZ")

set(CPACK_SOURCE_IGNORE_FILES
    ~$
    /CMakeLists.txt.user
    /devtools/deploy
    /devtools/release
    /devtools/sandboxes
    /.git/
    /.gitignore
    /.gitlab-ci.yml
    /build/
    /debug/
)
