//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/coregui/DataLoaders/QREDataLoaderResultModel.cpp
//! @brief     Implements class QREDataLoaderResultModel
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/coregui/DataLoaders/QREDataLoaderResultModel.h"

QREDataLoaderResultModel::QREDataLoaderResultModel(QREDataLoader::ImportResult* importResult)
    : m_importResult(importResult)
{
}

bool QREDataLoaderResultModel::rowHasError(int row) const
{
    return m_importResult->calculationErrors.contains(row);
}

int QREDataLoaderResultModel::rowCount() const
{
    return m_importResult->lines.size();
}

bool QREDataLoaderResultModel::rowIsSkipped(int row) const
{
    if (row >= 0 && row < m_importResult->lines.size())
        return m_importResult->lines[row].first;

    return false;
}

QString QREDataLoaderResultModel::headerTextOfCalculatedColumn(int column) const
{
    switch (column) {
    case 0:
        return "Q [1/nm]";
    case 1:
        return "R";
    case 2:
        return "sigma_R";
    }
    return QString();
}

int QREDataLoaderResultModel::columnCount(ColumnType type) const
{
    switch (type) {
    case ColumnType::line:
        return 1;
    case ColumnType::fileContent:
        return 1;
    case ColumnType::raw:
        return m_importResult->maxColumnCount;
    case ColumnType::processed: {
        const bool showErrorColumn =
            m_importResult->importSettings.columnDefinitions[QREDataLoader::DataType::dR].enabled;

        return showErrorColumn ? 3 : 2;
    }
    case ColumnType::error:
        return m_importResult->calculationErrors.isEmpty() ? 0 : 1;
    default:
        return 0;
    }
}

QString QREDataLoaderResultModel::cellText(ColumnType type, int row, int col) const
{
    if (col < 0 || row < 0 || row >= m_importResult->lines.size())
        return QString();

    if (type == ColumnType::fileContent)
        return (col == 0) ? m_importResult->lines[row].second : QString();

    const bool isSkippedLine = m_importResult->lines[row].first;
    if (isSkippedLine)
        return QString();

    switch (type) {
    case ColumnType::raw: {
        if (row >= m_importResult->rawValues.size())
            return QString();
        const auto& d = m_importResult->rawValues[row];
        return (col < d.size()) ? QString::number(d[col]) : QString();
    }

    case ColumnType::processed:
        if (col == 0)
            return (row < m_importResult->qValues.size())
                       ? QString::number(m_importResult->qValues[row])
                       : QString();
        if (col == 1)
            return (row < m_importResult->rValues.size())
                       ? QString::number(m_importResult->rValues[row])
                       : QString();
        if (col == 2)
            return (row < m_importResult->eValues.size())
                       ? QString::number(m_importResult->eValues[row])
                       : QString();
        return QString();

    case ColumnType::error:
        return (col == 0) ? m_importResult->errorText(row) : QString();

    default:
        return QString();
    }
}
