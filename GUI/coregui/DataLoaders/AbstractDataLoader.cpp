//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/coregui/DataLoaders/AbstractDataLoader.cpp
//! @brief     Implements class AbstractDataLoader
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/coregui/DataLoaders/AbstractDataLoader.h"
#include <QString>

void AbstractDataLoader::populateImportSettingsWidget(QWidget*) {}

void AbstractDataLoader::applyImportSettings() {}

void AbstractDataLoader::initWithDefaultImportSettings() {}

QByteArray AbstractDataLoader::serialize() const
{
    return QByteArray();
}

void AbstractDataLoader::deserialize(const QByteArray&) {}

QByteArray AbstractDataLoader::defaultImportSettings() const
{
    std::unique_ptr<AbstractDataLoader> cloned(clone());
    cloned->initWithDefaultImportSettings();
    return cloned->serialize();
}

void AbstractDataLoader::guessSettings() {}

int AbstractDataLoader::numErrors() const
{
    return 0;
}

int AbstractDataLoader::numLineRelatedErrors() const
{
    return 0;
}

QStringList AbstractDataLoader::lineUnrelatedErrors() const
{
    return {};
}

AbstractDataLoaderResultModel* AbstractDataLoader::createResultModel() const
{
    return nullptr;
}

QByteArray AbstractDataLoader::fileContent() const
{
    return {};
}

void AbstractDataLoader::setRealDataItem(RealDataItem* item)
{
    m_item = item;
}

RealDataItem* AbstractDataLoader::realDataItem()
{
    return m_item;
}

const RealDataItem* AbstractDataLoader::realDataItem() const
{
    return m_item;
}

QDataStream& operator<<(QDataStream& stream, const AbstractDataLoader& s)
{
    stream << s.serialize();
    return stream;
}

QDataStream& operator>>(QDataStream& stream, AbstractDataLoader& s)
{
    QByteArray b;
    stream >> b;
    s.deserialize(b);
    return stream;
}
