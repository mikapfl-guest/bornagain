//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/coregui/DataLoaders/QREDataLoaderResultModel.h
//! @brief     Defines class QREDataLoaderResultModel
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef GUI_COREGUI_DATALOADERS_QREDATALOADERRESULTMODEL_H
#define GUI_COREGUI_DATALOADERS_QREDATALOADERRESULTMODEL_H

#include "GUI/coregui/DataLoaders/AbstractDataLoaderResultModel.h"
#include "GUI/coregui/DataLoaders/QREDataLoader.h"

//! The result model of a QREDataLoader (for showing the import results in a table view).
class QREDataLoaderResultModel : public AbstractDataLoaderResultModel {
public:
    QREDataLoaderResultModel(QREDataLoader::ImportResult* importResult);

protected:
    virtual bool rowIsSkipped(int row) const override;
    virtual QString headerTextOfCalculatedColumn(int column) const override;
    virtual int columnCount(ColumnType type) const override;
    virtual QString cellText(ColumnType type, int row, int col) const override;
    virtual bool rowHasError(int row) const override;
    virtual int rowCount() const override;

private:
    QREDataLoader::ImportResult* m_importResult;
};

#endif // GUI_COREGUI_DATALOADERS_QREDATALOADERRESULTMODEL_H
