//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/coregui/DataLoaders/AutomaticDataLoader1D.h
//! @brief     Defines class AutomaticDataLoader1D
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_COREGUI_DATALOADERS_AUTOMATICDATALOADER1D_H
#define BORNAGAIN_GUI_COREGUI_DATALOADERS_AUTOMATICDATALOADER1D_H

#include "Device/Data/OutputData.h"
#include "GUI/coregui/DataLoaders/AbstractDataLoader1D.h"

//! Implements the legacy importer from BornAgain with no user interaction
class AutomaticDataLoader1D : public AbstractDataLoader1D {
public:
    virtual QString name() const override;
    virtual QString persistentClassName() const override;
    virtual AbstractDataLoader* clone() const override;
    virtual QByteArray serialize() const override;
    virtual void deserialize(const QByteArray& data) override;
    virtual void setFileContents(const QByteArray& fileContent) override;
    virtual QByteArray fileContent() const override;
    virtual void processContents() override;
    virtual AbstractDataLoaderResultModel* createResultModel() const override;
    virtual int numErrors() const override;
    virtual QStringList lineUnrelatedErrors() const override;

private:
    QByteArray m_fileContent;
    QString m_error;
};

#endif // BORNAGAIN_GUI_COREGUI_DATALOADERS_AUTOMATICDATALOADER1D_H
