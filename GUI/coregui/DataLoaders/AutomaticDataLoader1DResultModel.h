//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/coregui/DataLoaders/AutomaticDataLoader1DResultModel.h
//! @brief     Defines class AutomaticDataLoader1DResultModel
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef GUI_COREGUI_DATALOADERS_AUTOMATICDATALOADER1DRESULTMODEL_H
#define GUI_COREGUI_DATALOADERS_AUTOMATICDATALOADER1DRESULTMODEL_H

#include "GUI/coregui/DataLoaders/AbstractDataLoaderResultModel.h"

class RealDataItem;

//! The result model of a AutomaticDataLoader1D (for showing the import results in a table view).
class AutomaticDataLoader1DResultModel : public AbstractDataLoaderResultModel {
public:
    AutomaticDataLoader1DResultModel(RealDataItem* item);

protected:
    virtual bool rowIsSkipped(int row) const override;
    virtual QString headerTextOfCalculatedColumn(int column) const override;
    virtual int columnCount(ColumnType type) const override;
    virtual QString cellText(ColumnType type, int row, int col) const override;
    virtual bool rowHasError(int row) const override;
    virtual int rowCount() const override;

private:
    RealDataItem* m_item;
};

#endif // GUI_COREGUI_DATALOADERS_AUTOMATICDATALOADER1DRESULTMODEL_H
