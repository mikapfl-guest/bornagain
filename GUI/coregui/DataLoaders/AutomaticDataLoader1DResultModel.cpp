//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/coregui/DataLoaders/AutomaticDataLoader1DResultModel.cpp
//! @brief     Implements class AutomaticDataLoader1DResultModel
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/coregui/DataLoaders/AutomaticDataLoader1DResultModel.h"
#include "GUI/coregui/Models/RealDataItem.h"
#include "GUI/coregui/Models/SpecularDataItem.h"

AutomaticDataLoader1DResultModel::AutomaticDataLoader1DResultModel(RealDataItem* item)
    : m_item(item)
{
}

bool AutomaticDataLoader1DResultModel::rowHasError(int /*row*/) const
{
    return false;
}

int AutomaticDataLoader1DResultModel::rowCount() const
{
    auto data = m_item->specularDataItem()->getOutputData();
    if (!data)
        return 0;

    return int(data->getAllocatedSize());
}

bool AutomaticDataLoader1DResultModel::rowIsSkipped(int /*row*/) const
{
    return false;
}

QString AutomaticDataLoader1DResultModel::headerTextOfCalculatedColumn(int column) const
{
    switch (column) {
    case 0:
        return "Q [1/nm]";
    case 1:
        return "R";
    }
    return QString();
}

int AutomaticDataLoader1DResultModel::columnCount(ColumnType type) const
{
    return (type == ColumnType::processed) ? 2 : 0;
}

QString AutomaticDataLoader1DResultModel::cellText(ColumnType type, int row, int col) const
{
    if (col < 0 || row < 0 || row >= rowCount() || type != ColumnType::processed)
        return QString();

    auto data = m_item->specularDataItem()->getOutputData();

    if (col == 0)
        return QString::number(data->getAxisValue(row, 0));

    if (col == 1)
        return QString::number(data->operator[](row));

    return QString();
}
