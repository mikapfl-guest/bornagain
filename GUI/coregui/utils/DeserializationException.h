//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/coregui/utils/DeserializationException.h
//! @brief     Defines class DeserializationException
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_COREGUI_UTILS_DESERIALIZATIONEXCEPTION_H
#define BORNAGAIN_GUI_COREGUI_UTILS_DESERIALIZATIONEXCEPTION_H

#include <QString>

class DeserializationException {
private:
    DeserializationException(const QString& t);

public:
    static DeserializationException tooOld();
    static DeserializationException tooNew();
    static DeserializationException streamError();

    QString text() const;

private:
    QString m_text;
};

#endif // BORNAGAIN_GUI_COREGUI_UTILS_DESERIALIZATIONEXCEPTION_H
