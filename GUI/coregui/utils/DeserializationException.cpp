//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/coregui/utils/DeserializationException.cpp
//! @brief     Implements class DeserializationException
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2021
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#include "GUI/coregui/utils/DeserializationException.h"
#include <QString>

DeserializationException::DeserializationException(const QString& t) : m_text(t) {}

DeserializationException DeserializationException::tooOld()
{
    return DeserializationException("The found file is too old.");
}

DeserializationException DeserializationException::tooNew()
{
    return DeserializationException("The found file is too new.");
}

DeserializationException DeserializationException::streamError()
{
    return DeserializationException("The data seems to be corrupted.");
}

QString DeserializationException::text() const
{
    return m_text;
}
