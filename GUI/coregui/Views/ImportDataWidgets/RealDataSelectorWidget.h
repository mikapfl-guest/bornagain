//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/coregui/Views/ImportDataWidgets/RealDataSelectorWidget.h
//! @brief     Defines class RealDataSelectorWidget
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_COREGUI_VIEWS_IMPORTDATAWIDGETS_REALDATASELECTORWIDGET_H
#define BORNAGAIN_GUI_COREGUI_VIEWS_IMPORTDATAWIDGETS_REALDATASELECTORWIDGET_H

#include <QWidget>

class RealDataPropertiesWidget;
class RealDataItemSelectorWidget;
class InstrumentModel;
class RealDataModel;
class SessionItem;
class RealDataSelectorActions;
class RealDataItem;
class RealDataTreeModel;
class QItemSelection;
class QTreeView;

//! The RealDataSelectorWidget represents left panel of ImportDataView. Contains a tree to
//! select data set (m_itemTree & m_itemTreeModel) and properties of currently selected data
//! (RealDataPropertiesWidget).

class RealDataSelectorWidget : public QWidget {
    Q_OBJECT

public:
    RealDataSelectorWidget(QWidget* parent = 0);

    virtual QSize sizeHint() const override;
    virtual QSize minimumSizeHint() const override;

    void setModel(RealDataModel* realDataModel);

    RealDataItem* currentItem();
    void setCurrentItem(RealDataItem* item);

signals:
    void selectionChanged(SessionItem*);

protected:
    virtual void showEvent(QShowEvent*) override;

private:
    void onSelectionChanged();
    void renameCurrentItem();
    void removeCurrentItem();
    void rotateCurrentItem();
    void updateActionEnabling();
    void updateActionEnabling(const RealDataItem* item);
    void onContextMenuRequest(const QPoint& point);
    void importData(int ndim);
    QModelIndex currentIndex();

private:
    QTreeView* m_itemTree;
    RealDataTreeModel* m_itemTreeModel;
    RealDataPropertiesWidget* m_propertiesWidget;
    QAction* m_import2dDataAction;
    QAction* m_import1dDataAction;
    QAction* m_renameDataAction;
    QAction* m_removeDataAction;
    QAction* m_rotateDataAction;
};

#endif // BORNAGAIN_GUI_COREGUI_VIEWS_IMPORTDATAWIDGETS_REALDATASELECTORWIDGET_H
