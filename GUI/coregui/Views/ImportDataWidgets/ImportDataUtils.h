//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/coregui/Views/ImportDataWidgets/ImportDataUtils.h
//! @brief     Defines ImportDataUtils namespace
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_COREGUI_VIEWS_IMPORTDATAWIDGETS_IMPORTDATAUTILS_H
#define BORNAGAIN_GUI_COREGUI_VIEWS_IMPORTDATAWIDGETS_IMPORTDATAUTILS_H

#include "GUI/coregui/utils/ImportDataInfo.h"
#include <QString>
#include <memory>
#include <vector>

template <class T> class OutputData;
class RealDataItem;
class InstrumentItem;
class GISASInstrumentItem;
class AbstractDataLoader;

//! Provides utility methods to import data files.

namespace ImportDataUtils {
std::unique_ptr<OutputData<double>> Import2dData(const QString& baseNameOfLoadedFile);

//! Import 1D data into the given item.
//! Returns an error text if a fatal error occurred (discard item). "Empty string" means "no fatal
//! error" => imported item should be kept. The imported item still can have errors, but they might
//! be solvable by different import settings or by using a different data loader.
//! selectedLoader is the one which was selected in the open-file-dialog (or null if none selected).
QString Import1dData(RealDataItem* realDataItem, const AbstractDataLoader* selectedLoader);

std::unique_ptr<OutputData<double>> ImportKnownData(const QString& baseNameOfLoadedFile);
std::unique_ptr<OutputData<double>> ImportReflectometryData(const QString& baseNameOfLoadedFile);

//! Creates OutputData with bin-valued axes.
std::unique_ptr<OutputData<double>> CreateSimplifiedOutputData(const OutputData<double>& data);

//! Check whether data item is compatible with instrument (same rank)
bool Compatible(const InstrumentItem& instrumentItem, const RealDataItem& realDataItem);

//! Composes a message with the shapes of InstrumentItem and RealDataItem.
QString printShapeMessage(const std::vector<int>& instrument_shape,
                          const std::vector<int>& data_shape);
}; // namespace ImportDataUtils

#endif // BORNAGAIN_GUI_COREGUI_VIEWS_IMPORTDATAWIDGETS_IMPORTDATAUTILS_H
