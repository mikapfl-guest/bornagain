//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      GUI/coregui/Views/SpecularDataWidgets/SpecularDataCanvas.h
//! @brief     Defines class SpecularDataCanvas
//!
//! @homepage  http://www.bornagainproject.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2018
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef BORNAGAIN_GUI_COREGUI_VIEWS_SPECULARDATAWIDGETS_SPECULARDATACANVAS_H
#define BORNAGAIN_GUI_COREGUI_VIEWS_SPECULARDATAWIDGETS_SPECULARDATACANVAS_H

#include "GUI/coregui/Views/CommonWidgets/SessionItemWidget.h"
#include <QWidget>

class SpecularDataItem;
class SpecularPlotCanvas;
class QCustomPlot;

class SpecularDataCanvas : public SessionItemWidget {
    Q_OBJECT
public:
    explicit SpecularDataCanvas(QWidget* parent = nullptr);

    void setItem(SessionItem* intensityItem) override;

    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;

    QList<QAction*> actionList() override;
    QCustomPlot* customPlot();

    //! Enable or disable the onMousePress() handler.
    //! For legacy reasons the onMousePress handler is enabled.
    //! It is deprecated because of:
    //! * mousepress is the wrong event to listen to for opening a context menu
    //! * disables possibility to get context menu events for specific parts of the plot (e.g.
    //! context menu for axis configuration)
    void enableDeprecatedOnMousePress(bool b);

public slots:
    void onResetViewAction();
    void onSavePlotAction();
    void onMousePress(QMouseEvent* event);

private:
    SpecularDataItem* specularDataItem();
    void initActions();

    SpecularPlotCanvas* m_plot_canvas;
    QAction* m_reset_view_action;
    QAction* m_save_plot_action;
};

#endif // BORNAGAIN_GUI_COREGUI_VIEWS_SPECULARDATAWIDGETS_SPECULARDATACANVAS_H
